package ru.t1.ytarasov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.enumirated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Project {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @NotNull
    private Date updated = new Date();

    private Date dateStart;

    private Date dateFinish;

    public Project(@NotNull String name) {
        this.name = name;
    }

    public Project(@NotNull String name, String description) {
        this.name = name;
        this.description = description;
    }

}
