package ru.t1.ytarasov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.enumirated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @NotNull
    private Date updated = new Date();

    private Date dateStart;

    private Date dateFinish;

    private String projectId;

    public Task(@NotNull String name) {
        this.name = name;
    }

    public Task(@NotNull String name, String description) {
        this.name = name;
        this.description = description;
    }

}
