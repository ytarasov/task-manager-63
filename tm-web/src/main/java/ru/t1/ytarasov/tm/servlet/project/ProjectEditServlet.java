package ru.t1.ytarasov.tm.servlet.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.util.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final Project project = ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String dateStartValue = req.getParameter("dateStart");
        @NotNull final String dateFinishValue = req.getParameter("dateFinish");
        @NotNull final Status status = Status.valueOf(req.getParameter("status"));
        final Date dateStart = DateUtil.toDate(dateStartValue);
        final Date dateFinish = DateUtil.toDate(dateFinishValue);
        @NotNull final Project project = ProjectRepository.getInstance().findById(id);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        project.setStatus(status);
        project.setUpdated(new Date());
        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}
