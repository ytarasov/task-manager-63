package ru.t1.ytarasov.tm.servlet.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;
import ru.t1.ytarasov.tm.util.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        req.setAttribute("task", TaskRepository.getInstance().findById(id));
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String dateStartValue = req.getParameter("dateStart");
        @NotNull final String dateFinishValue = req.getParameter("dateFinish");
        @NotNull final Status status = Status.valueOf(req.getParameter("status"));
        @NotNull final String projectId = req.getParameter("projectId");
        final Date dateStart = DateUtil.toDate(dateStartValue);
        final Date dateFinish = DateUtil.toDate(dateFinishValue);
        @NotNull final Task task = TaskRepository.getInstance().findById(id);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);
        task.setProjectId(projectId);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
