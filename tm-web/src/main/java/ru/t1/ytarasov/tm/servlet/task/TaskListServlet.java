package ru.t1.ytarasov.tm.servlet.task;

import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/tasks")
public class TaskListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("tasks", TaskRepository.getInstance().findAll());
        req.setAttribute("projectRepository", ProjectRepository.getInstance());
        req.getRequestDispatcher("/WEB-INF/views/tasks.jsp").forward(req, resp);
    }

}
