package ru.t1.ytarasov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface EntityConstant {

    @NotNull
    String TABLE_PROJECT = "Project";

    @NotNull
    String TABLE_TASK = "Task";

    @NotNull
    String TABLE_USER = "User";

    @NotNull
    String TABLE_SESSION = "Session";

    @NotNull
    String COLUMN_ID = "id";

    @NotNull
    String COLUMN_NAME = "name";

    @NotNull
    String COLUMN_CREATED = "created";

    @NotNull
    String COLUMN_DESCRIPTION = "description";

    @NotNull
    String COLUMN_USER_ID = "user.id";

    @NotNull
    String COLUMN_STATUS = "status";

    @NotNull
    String COLUMN_ROLE = "role";

    @NotNull
    String COLUMN_PROJECT_ID = "project.id";

    @NotNull
    String COLUMN_LOGIN = "login";

    @NotNull
    String COLUMN_PASSWORD = "passwordHash";

    @NotNull
    String COLUMN_EMAIL = "email";

    @NotNull
    String COLUMN_LOCKED = "locked";

    @NotNull
    String COLUMN_FIRST_NAME = "firstName";

    @NotNull
    String COLUMN_LAST_NAME = "lastName";

    @NotNull
    String COLUMN_MIDDLE_NAME = "middleName";

    @NotNull
    String COLUMN_DATE = "date";

}
