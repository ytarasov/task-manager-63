package ru.t1.ytarasov.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDtoService<M> implements IUserOwnedDtoService<M> {
}
