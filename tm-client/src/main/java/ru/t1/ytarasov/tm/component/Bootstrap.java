package ru.t1.ytarasov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.api.model.IListener;
import ru.t1.ytarasov.tm.api.service.ILoggerService;
import ru.t1.ytarasov.tm.event.ConsoleEvent;
import ru.t1.ytarasov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ytarasov.tm.listener.AbstractListener;
import ru.t1.ytarasov.tm.service.LoggerService;
import ru.t1.ytarasov.tm.util.SystemUtil;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    private void prepareStart() {
        initPid();
        initLogger();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

    public void run(@Nullable String[] args) {
        if (runArguments(args)) System.exit(0);
        prepareStart();
        System.out.println();
        runCommands();
    }

    private boolean runArguments(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        try {
            runArgument(arg);
        } catch (@Nullable final Exception e) {
            loggerService.error(e);
            System.out.println();
        }
        return true;
    }

    @Nullable
    private AbstractListener getArgumentFromListener(@NotNull final String arg) {
        for (AbstractListener listener : listeners) {
            if (arg.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    @SneakyThrows
    private void runArgument(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedException();
        @Nullable final AbstractListener listener = getArgumentFromListener(arg);
        if (listener == null) throw new ArgumentNotSupportedException(arg);
        publisher.publishEvent(new ConsoleEvent(listener.getName()));
    }

    private void runCommands() {
        while (!Thread.currentThread().isInterrupted()) runCommand();
    }

    private void runCommand() {
        try {
            @Nullable String command = TerminalUtil.nextLine();
            loggerService.command(command);
            publisher.publishEvent(new ConsoleEvent(command));
            System.out.println("[OK]");
        } catch (@Nullable final Exception e) {
            loggerService.error(e);
            System.out.println();
            System.out.println("[FAIL]");
        }
    }

    @SneakyThrows
    public void runCommand(@Nullable final String command) {
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO THE TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}
